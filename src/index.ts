import express, { Application, Request, Response, NextFunction } from 'express';
import cors from 'cors';


const app: Application = express();

app.use(cors())
app.use(express.json());
app.use(express.urlencoded({ extended: false }));


app.get('/', (req : Request, res : Response) => {
    res.send('Hello World!');
})

app.listen(5001, () => {   
    console.log('tim kucing be app  listening on port 5001!');
})




